package ee.valiit.companiesapi;

import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.repository.CompaniesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

@SpringBootTest
class CompaniesapiApplicationTests {

    @Autowired
    private CompaniesRepository companiesRepository;

    @Test
    public void testLoadCompanies() {
        List<Company> companies = companiesRepository.getAllCompanies();

        Assertions.assertTrue(companies.size() > 0);
    }

    @Test
    public void testManipulateCompanies() {
        Company company = new Company(
        		0, "Unit Test OÜ", "https://unittest.org/logo.png",
				"2020-05-31", 234, Collections.emptyList());

        int companyId = companiesRepository.addCompany(company);

        Assertions.assertTrue(companyId > 0);

        Company loadedCompany = companiesRepository.getCompany(companyId);

        Assertions.assertNotNull(loadedCompany);
        Assertions.assertEquals("Unit Test OÜ", loadedCompany.getName());

        companiesRepository.deleteCompany(companyId);

		loadedCompany = companiesRepository.getCompany(companyId);
		Assertions.assertNull(loadedCompany);

    }

}
