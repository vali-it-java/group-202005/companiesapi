package ee.valiit.companiesapi.controller;

import ee.valiit.companiesapi.dto.GenericResponseDto;
import ee.valiit.companiesapi.dto.JwtResponseDto;
import ee.valiit.companiesapi.dto.UserRegistrationDto;
import ee.valiit.companiesapi.dto.UsernamePasswordDto;
import ee.valiit.companiesapi.model.User;
import ee.valiit.companiesapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody UsernamePasswordDto request) throws Exception {
        return userService.authenticate(request);
    }

    @GetMapping("/currentUser")
    public User getCurrentUser() {
        return userService.getCurrentUser();
    }
}
