package ee.valiit.companiesapi.controller;

import ee.valiit.companiesapi.model.Announcement;
import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.model.Dog;
import ee.valiit.companiesapi.model.OperationResult;
import ee.valiit.companiesapi.repository.CompaniesRepository;
import ee.valiit.companiesapi.service.AnnouncementService;
import ee.valiit.companiesapi.service.CompaniesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
@CrossOrigin("*")
public class CompaniesController {

    @Autowired
    private CompaniesRepository companiesRepository;

    @Autowired
    private CompaniesService companiesService;

    @Autowired
    private AnnouncementService announcementService;

    @GetMapping("/hello/{name}")
    public String getHelloWorld(@PathVariable("name") String name) {
        return "Hello, " + name + "!";
    }

    @GetMapping("/dog")
    public Dog[] getDog() {
        Dog myDog = new Dog();
        myDog.setName("Muki");
        myDog.setTailLength(56);

        Dog myDog2 = new Dog();
        myDog2.setName("Rex");
        myDog2.setTailLength(123);

        return new Dog[] { myDog, myDog2 };
    }

    // get - all companies
    @GetMapping("/all")
    public List<Company> getCompanies() {
        return companiesRepository.getAllCompanies();
    }

    // Backend...
    // Get - single company by id
    @GetMapping("/{companyId}")
    public Company getSingleCompany(@PathVariable("companyId") int id) {
        return companiesRepository.getCompany(id);
    }

    // POST - by name
    @PostMapping("/all")
    public List<Company> getCompaniesByName(@RequestParam(name = "companyName", defaultValue = "") String name) {
        return companiesRepository.getAllCompaniesByName(name);
    }

    // DELETE - delete single company by
    @DeleteMapping("/{companyId}")
    public ResponseEntity<OperationResult> deleteCompany(@PathVariable("companyId") int id) {
        OperationResult result = companiesService.deleteCompany(id);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    // POST - add company
    @PostMapping("/add")
    public ResponseEntity<OperationResult> addCompany(@RequestBody Company company) {
        OperationResult result = companiesService.addCompany(company);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    // PUT - update company
    @PutMapping("/update")
    public ResponseEntity<OperationResult> updateCompany(@RequestBody Company company){
        OperationResult result = companiesService.updateCompany(company);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/announcements/add/{companyId}")
    public ResponseEntity<OperationResult> addAnnouncement(
            @RequestBody Announcement announcement, @PathVariable int companyId) {
        OperationResult result = announcementService.addAnnouncement(announcement, companyId);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/announcements/update")
    public ResponseEntity<OperationResult> updateAnnouncement(@RequestBody Announcement announcement) {
        OperationResult result = announcementService.updateAnnouncement(announcement);
        if (result.isSuccess()) {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(result, new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }
}
