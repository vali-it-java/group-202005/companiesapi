package ee.valiit.companiesapi.repository;

import ee.valiit.companiesapi.model.Announcement;
import ee.valiit.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class AnnouncementRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Announcement> getAnnouncements(int companyId) {
        return jdbcTemplate.query(
                "select * from announcement where company_id = ?",
                new Object[]{companyId},
                (row, number) -> new Announcement(
                        row.getInt("id"),
                        row.getString("title"),
                        row.getString("body")
                )
        );
    }

    public int addAnnouncement(Announcement announcement, int companyId) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(
                    "insert into announcement (`title`, `body`, `company_id`) values (?, ?, ?)",
                    Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, announcement.getTitle());
            ps.setString(2, announcement.getBody());
            ps.setInt(3, companyId);
            return ps;
        }, keyHolder);
        return keyHolder.getKey().intValue();
    }

    public void updateAnnouncement(Announcement announcement) {
        jdbcTemplate.update(
                "update `announcement` set `title` = ?, `body` = ? where `id` = ?",
                announcement.getTitle(), announcement.getBody(), announcement.getId()
        );
    }
}
