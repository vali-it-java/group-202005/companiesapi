package ee.valiit.companiesapi.service;

import ee.valiit.companiesapi.model.Announcement;
import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.model.OperationResult;
import ee.valiit.companiesapi.repository.AnnouncementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class AnnouncementService {

    @Autowired
    private AnnouncementRepository announcementRepository;

    public OperationResult addAnnouncement(Announcement announcement, int companyId) {
        try {
            Assert.isTrue(announcement != null, "Announcement does not exist!");
            Assert.isTrue(announcement.getTitle() != null && !announcement.getTitle().equals(""), "Title not specified!");
            Assert.isTrue(announcement.getBody() != null && !announcement.getBody().equals(""), "Body not specified!");
            int announcementId = announcementRepository.addAnnouncement(announcement, companyId);
            return new OperationResult(true, "OK", announcementId);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResult(false, message, null);
        }
    }

    public OperationResult updateAnnouncement(Announcement announcement) {
        try {
            Assert.isTrue(announcement != null, "Announcement does not exist!");
            Assert.isTrue(announcement.getId() > 0, "Id not specified!");
            Assert.isTrue(announcement.getTitle() != null && !announcement.getTitle().equals(""), "Title not specified!");
            Assert.isTrue(announcement.getBody() != null && !announcement.getBody().equals(""), "Body not specified!");
            announcementRepository.updateAnnouncement(announcement);
            return new OperationResult(true, "OK", announcement.getId());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            String message = e.getMessage();
            return new OperationResult(false, message, null);
        }
    }
}
