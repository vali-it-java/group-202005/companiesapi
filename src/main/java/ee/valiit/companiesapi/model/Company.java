package ee.valiit.companiesapi.model;

import java.util.List;

public class Company {
    private int id;
    private String name;
    private String logo;
    private String established;
    private int employees;
    private List<Announcement> announcements;

    public Company() {
    }

    public Company(int id, String name, String logo, String established, int employees, List<Announcement> announcements) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.established = established;
        this.employees = employees;
        this.announcements = announcements;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEstablished() {
        return established;
    }

    public void setEstablished(String established) {
        this.established = established;
    }

    public int getEmployees() {
        return employees;
    }

    public void setEmployees(int employees) {
        this.employees = employees;
    }

    public List<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(List<Announcement> announcements) {
        this.announcements = announcements;
    }
}
